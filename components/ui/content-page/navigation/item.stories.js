import UiNavigationItem from '@/components/ui/content-page/navigation/item.vue';

export default {
  title: 'content-page-navigation-item',
  component: UiNavigationItem
}

export const NavigationItem = (args, { argTypes }) => ({
  components: { UiNavigationItem },
  props: Object.keys(argTypes),
  template: '<ui-content-page-navigation-item v-bind="$props"/>'
});
