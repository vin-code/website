import UiNavigation from '@/components/ui/content-page/navigation.vue';

export default {
  title: 'content-page-navigation',
  component: UiNavigation
}

export const Navigation = (args, { argTypes }) => ({
  components: { UiNavigation },
  props: Object.keys(argTypes),
  template: '<ui-content-page-navigation v-bind="$props"/>'
});
