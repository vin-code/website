import UIFooterNavigation from '@/components/ui/footer-navigation.vue';

export default {
  title: 'footer-navigation',
  component: UIFooterNavigation
}

export const FooterNavigation = (args, { argTypes }) => ({
  components: { UIFooterNavigation },
  props: Object.keys(argTypes),
  template: '<ui-footer-navigation v-bind="$props"/>'
});
