import UIBeatingIcon from '@/components/ui/beating-icon.vue';

export default {
  title: 'beating-icon',
  component: UIBeatingIcon
}

export const BeatingIcon = (args, { argTypes }) => ({
  components: { UIBeatingIcon },
  props: Object.keys(argTypes),
  template: '<ui-beating-icon v-bind="$props"/>'
});
