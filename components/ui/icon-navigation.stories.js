import UIIconNavigation from '@/components/ui/icon-navigation.vue';

export default {
  title: 'icon-navigation',
  component: UIIconNavigation,
  argTypes: {
    small: { control: 'boolean', defaultValue: false },
  }
}

export const IconNavigation = (args, { argTypes }) => ({
  components: { UIIconNavigation },
  props: Object.keys(argTypes),
  template: '<ui-icon-navigation v-bind="$props"/>'
});
