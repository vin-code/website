import UIAnimatedBackground from '@/components/ui/animated-background/icon.vue';

export default {
  title: 'animated-background-icon',
  component: UIAnimatedBackground
}

export const TypingCursor = (args, { argTypes }) => ({
  components: { UIAnimatedBackground },
  props: Object.keys(argTypes),
  template: '<ui-animated-background-icon v-bind="$props"/>'
});
