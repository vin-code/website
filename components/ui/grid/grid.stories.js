import UIGridLayout from '@/components/ui/grid/layout.vue';
import UIGridCell from '@/components/ui/grid/cell.vue';

export default {
  title: 'grid-layout',
  component: UIGridLayout
}

export const GridLayout = (args, { argTypes }) => ({
  components: { UIGridLayout, UIGridCell },
  props: Object.keys(argTypes),
  template: `
    <ui-grid-layout>
      <ui-grid-cell> test 1 </ui-grid-cell>
      <ui-grid-cell :cols="{ xs: 6 }"> test 2 </ui-grid-cell>
      <ui-grid-cell :cols="{ xs: 6 }" :rows="{ xs: 2 }"> test 3 </ui-grid-cell>
      <ui-grid-cell :cols="{ xs: 6 }"> test 4 </ui-grid-cell>
    </ui-grid-layout>
  `
});
