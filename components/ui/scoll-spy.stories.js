import UIScrollSpy from '@/components/ui/scroll-spy.vue';

export default {
  title: 'scroll-spy',
  component: UIScrollSpy
}

export const ScrollSpy = (args, { argTypes }) => ({
  components: { UIScrollSpy },
  props: Object.keys(argTypes),
  template: '<ui-scroll-spy v-bind="$props"/>'
});
