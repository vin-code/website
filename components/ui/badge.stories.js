import UIBadge from '@/components/ui/badge.vue';

export default {
  title: 'badge',
  component: UIBadge
}

export const Badge = (args, { argTypes }) => ({
  components: { UIBadge },
  props: Object.keys(argTypes),
  template: '<ui-badge v-bind="$props"/>'
});
