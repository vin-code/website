import UICopyright from '@/components/ui/copyright.vue';

export default {
  title: 'copyright',
  component: UICopyright
}

export const Copyright = (args, { argTypes }) => ({
  components: { UICopyright },
  props: Object.keys(argTypes),
  template: '<ui-copyright v-bind="$props"/>'
});
