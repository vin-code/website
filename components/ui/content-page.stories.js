import UIContentPage from '@/components/ui/content-page.vue';

export default {
  title: 'content-page',
  component: UIContentPage,
  argTypes: {
    small: { control: 'boolean', defaultValue: false },
  }
}

export const ContentPage = (args, { argTypes }) => ({
  components: { UIContentPage },
  props: Object.keys(argTypes),
  template: '<ui-content-page v-bind="$props"/>'
});
