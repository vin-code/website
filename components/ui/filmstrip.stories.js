import UIFilmstrip from '@/components/ui/header.vue';

export default {
  title: 'filmstrip',
  component: UIFilmstrip
}

export const Filmstrip = (args, { argTypes }) => ({
  components: { UIFilmstrip },
  props: Object.keys(argTypes),
  template: '<ui-filmstrip v-bind="$props"/>'
});
