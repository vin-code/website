import UIButtonRound from '@/components/ui/button-round.vue';

export default {
  title: 'button-round',
  component: UIButtonRound
}

export const ButtonRound = (args, { argTypes }) => ({
  components: { UIButtonRound },
  props: Object.keys(argTypes),
  template: '<ui-button-round v-bind="$props"/>'
});
