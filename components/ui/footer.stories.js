import UIFooter from '@/components/ui/footer.vue';

export default {
  title: 'footer',
  component: UIFooter
}

export const Footer = (args, { argTypes }) => ({
  components: { UIFooter },
  props: Object.keys(argTypes),
  template: '<ui-footer v-bind="$props"/>'
});
