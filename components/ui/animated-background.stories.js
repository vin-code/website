import UIAnimatedBackground from '@/components/ui/animated-background.vue';

export default {
  title: 'animated-background',
  component: UIAnimatedBackground
}

export const AnimatedBackground = (args, { argTypes }) => ({
  components: { UIAnimatedBackground },
  props: Object.keys(argTypes),
  template: '<ui-animated-background v-bind="$props"/>'
});
