import UILogo from '@/components/ui/logo.vue';

export default {
  title: 'logo',
  component: UILogo
}

export const Logo = (args, { argTypes }) => ({
  components: { UILogo },
  props: Object.keys(argTypes),
  template: '<ui-logo v-bind="$props"/>'
});
