import UIAvatar from '@/components/ui/avatar.vue';

export default {
  title: 'avatar',
  component: UIAvatar
}

export const Avatar = (args, { argTypes }) => ({
  components: { UIAvatar },
  props: Object.keys(argTypes),
  template: '<ui-avatar v-bind="$props"/>'
});
