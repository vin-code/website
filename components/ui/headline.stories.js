import UIHeadline from '@/components/ui/headline.vue';

export default {
  title: 'headline',
  component: UIHeadline,
}

export const Headline = (args, { argTypes }) => ({
  components: { UIHeadline },
  props: Object.keys(argTypes),
  template: '<ui-headline v-bind="$props"/>'
});
