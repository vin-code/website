import UIHeader from '@/components/ui/header.vue';

export default {
  title: 'header',
  component: UIHeader,
  argTypes: {
    small: { control: 'boolean', defaultValue: false },
  }
}

export const Header = (args, { argTypes }) => ({
  components: { UIHeader },
  props: Object.keys(argTypes),
  template: '<ui-header v-bind="$props"/>'
});
