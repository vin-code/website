import UITypingCursor from '@/components/ui/typing/cursor.vue';

export default {
  title: 'typing-cursor',
  component: UITypingCursor
}

export const TypingCursor = (args, { argTypes }) => ({
  components: { UITypingCursor },
  props: Object.keys(argTypes),
  template: '<ui-typing-cursor v-bind="$props"/>'
});
