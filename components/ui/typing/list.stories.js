import UITypingList from '@/components/ui/typing/list.vue';

export default {
  title: 'typing-list',
  component: UITypingList
}

export const TypingCursor = (args, { argTypes }) => ({
  components: { UITypingList },
  props: Object.keys(argTypes),
  template: '<ui-typing-list v-bind="$props"/>'
});
