export default (context, inject) => {
  inject(
    'scrollObserver',
    (
      target,
      callback,
      options
    ) => {
      if (!target) {
        throw new Error('Target ref for scroll observer need to be specified');
      }

      const observer = new IntersectionObserver(callback, options);

      observer.observe(target);
  })
}

export const buildThresholdList = (steps) => {
  if (!(steps >= 1)) {
    throw new Error('Steps need to be bigger or equal to 1');
  }

  if (steps - 1 === 0) {
    return [1];
  }

  const thresholds = [];
  const faktor = 1 / steps;
  let product = 0;

  thresholds.push(product);

  for (let multiplyer = 0; multiplyer <= steps; multiplyer++) {
    product = faktor * multiplyer;
    thresholds.push(product);
  }

  return thresholds;
}
